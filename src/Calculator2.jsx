import { useState } from 'react'

function c_to_c(c) {
  return c
}

function c_to_f(c) {
  return c * 2
}

function f_to_c(f) {
  return f / 2
}

let converters = {
  C: c_to_c,
  F: c_to_f,
}

let revert_converters = {
  C: c_to_c,
  F: f_to_c,
}

function TempInput({ unit, value, setValue }) {
  let converter = converters[unit]
  let revert_converter = revert_converters[unit]
  let temp = converter(value)
  return (
    <fieldset>
      <legend>Input temperature in {unit}</legend>
      <input
        type="number"
        value={temp}
        onChange={e => setValue(revert_converter(e.target.valueAsNumber))}
      />
    </fieldset>
  )
}
function Calculator() {
  const [value, setValue] = useState(1)
  return (
    <div>
      <TempInput unit="C" value={value} setValue={setValue} />
      <TempInput unit="F" value={value} setValue={setValue} />
    </div>
  )
}

export default Calculator
