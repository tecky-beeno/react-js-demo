import { useState } from 'react'

function c_to_c(c) {
  return c
}

function c_to_f(c) {
  return c * 2
}

function f_to_c(f) {
  return f / 2
}

function TempInput({ unit, value, handleOnChange }) {
  let fn = e => handleOnChange(e)
  return (
    <fieldset>
      <legend>Input temperature in {unit}</legend>
      <input type="number" value={value} onChange={fn} />
    </fieldset>
  )
}
function Calculator() {
  const [{ scale, temperature }, setState] = useState({
    scale: 'C',
    temperature: 100,
  })
  // function onCChange(temperature) {
  //   setState({ scale: 'C', temperature })
  // }
  // function onFChange(temperature) {
  //   setState({ scale: 'F', temperature })
  // }
  function handleCChange(e) {
    setState({ scale: 'C', temperature: e.target.valueAsNumber })
  }
  function handleFChange(e) {
    setState({ scale: 'F', temperature: e.target.valueAsNumber })
  }
  const c = scale == 'C' ? temperature : f_to_c(temperature)
  const f = scale == 'F' ? temperature : c_to_f(temperature)
  return (
    <div>
      <TempInput unit="C" value={c} handleOnChange={handleCChange} />
      <TempInput unit="F" value={f} handleOnChange={handleFChange} />
      <p>water {c >= 100 ? 'boil' : 'not boil'}</p>

      <div>{JSON.stringify({ temperature, scale })}</div>
    </div>
  )
}

export default Calculator
