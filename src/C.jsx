import React, { useContext, createContext } from 'react'

function C() {
  return (
    <div>
      <h1>Context</h1>
      <ThemeContext.Provider value={DefaultThemeValue}>
        <L1 />
      </ThemeContext.Provider>
    </div>
  )
}

function L1() {
  return <L2></L2>
}
function L2() {
  return (
    <div>
      <h2>L2</h2>

      <L3 />
    </div>
  )
}
function L3() {
  const value = useContext(ThemeContext)
  return (
    <>
      <h3>L3</h3>
      <div>value: {JSON.stringify(value)}</div>
      <ThemeContext.Consumer>
        {value => {
          return (
            <div>
              inside Consumer
              <div>value: {JSON.stringify(value)}</div>
            </div>
          )
        }}
      </ThemeContext.Consumer>
    </>
  )
}

const DefaultThemeValue = {
  text: { color: 'black', size: '12px' },
  background: { color: 'white' },
}
const ThemeContext = createContext()

window.React = React

Object.assign(window, { React, ThemeContext })

export default C
