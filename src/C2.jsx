import React, { useContext, createContext, memo, useState } from 'react'

function C() {
  const [theme, setTheme] = useState(DefaultThemeValue)
  return (
    <div>
      <h1>Context</h1>
      <ThemeContext.Provider value={{ theme, setTheme }}>
        <L1 />
      </ThemeContext.Provider>
    </div>
  )
}

let L1 = () => {
  return (
    <div>
      <h1>L1</h1>
      {new Date().toLocaleTimeString()}
      <L2 />
    </div>
  )
}
// let L1Memo = memo(L1)
L1 = memo(L1)

function L2() {
  return (
    <div>
      <h2>L2</h2>
      {new Date().toLocaleTimeString()}
      <L3 />
    </div>
  )
}
function L3() {
  const { theme, setTheme } = useContext(ThemeContext)
  return (
    <>
      <h3>L3</h3>
      {new Date().toLocaleTimeString()}
      <div>theme: {JSON.stringify(theme)}</div>
      <button
        style={{
          color: theme.text.color,
          backgroundColor: theme.background.color,
        }}
        onClick={() => setTheme(LightTheme)}
      >
        change to light theme
      </button>

      <button
        style={{
          color: theme.text.color,
          backgroundColor: theme.background.color,
        }}
        onClick={() => setTheme(DarkTheme)}
      >
        change to dark theme
      </button>
    </>
  )
}

const LightTheme = {
  text: { color: 'black', size: '12px' },
  background: { color: 'white' },
}
const DarkTheme = {
  text: { color: 'white', size: '12px' },
  background: { color: 'black' },
}

const DefaultThemeValue = LightTheme
const ThemeContext = createContext()

window.React = React

Object.assign(window, { React, ThemeContext })

export default C
