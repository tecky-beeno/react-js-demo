import { useState, useEffect } from 'react'

export function HC() {
  let array = ['apple', 'banana', 'cherry']
  // array = []
  return (
    <div>
      <h1>HC</h1>
      <List array={array} component={Item}></List>
      {/* <Profile json={{ username: 'Alice' }} /> */}

      <Get
        url="http://localhost:8080"
        component={Profile}
        fallback={{ username: 'Alice (offline)' }}
      />
    </div>
  )
}

function Loading({ url }) {
  return <div>loading {url} ...</div>
}
function Profile({ json: { username } }) {
  return (
    <div>
      <h2>Profile</h2>
      user: {username}
    </div>
  )
}
function ErrorMessage({ error }) {
  return <div style={{ color: 'red' }}>{String(error)}</div>
}

function Get({ url, component: Component, fallback }) {
  const [{ status, json, error }, setState] = useState({ status: 'loading' })
  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .catch(() => fallback)
      .then(json => setState({ json, status: 'success' }))
      .catch(error => setState({ error, status: 'fail' }))
  }, [url])
  return (
    <>
      {status === 'loading' ? (
        <Loading url={url} />
      ) : status === 'success' ? (
        <Component json={json} />
      ) : (
        <ErrorMessage error={error} />
      )}
    </>
  )
}

function List({ array, component: Component }) {
  return (
    <>
      {array.length > 0 ? (
        array.map((value, index) => <Component value={value} index={index} />)
      ) : (
        <p>Empty</p>
      )}
    </>
  )
}

function Item({ value, index }) {
  return (
    <div>
      <h3>Item {index}</h3>
      <p>{value}</p>
    </div>
  )
}

export default HC
